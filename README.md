# Chinese bear beaver + sadcat meme generator

## Setup venv:
```bash
python -m venv .venv
source ./.venv/bin/activate
pip install -r requirements.txt
```

## Generate meme

Add `background.mp4` clip to directory. (Can be different extension, change in code).
Run `python main.py`


## Meme with text

To use text in the meme, change False to True in line 48:
```diff
- all_texts(False)
+ all_texts(True)
```