import moviepy.editor as mpy

vcodec =   "libx264"
videoquality = "24"
compression = "slow"


def add_text(text, pos, time, dur=2):
    txt = mpy.TextClip(text,
                    font='Courier',
                    fontsize=120,
                    color='black',
                    transparent=True
                    )
    txt = txt.set_position(pos)
    txt = txt.set_start(time)
    txt = txt.set_duration(dur)
    txt = txt.crossfadein(0.5)
    txt = txt.crossfadeout(0.5)
    return txt


def all_texts(use_text=False):
    if not use_text:
        return []
    clips = []
    
    clips.append(add_text('meu1', (900, 800), (2)))
    clips.append(add_text('meu2', (950, 800), (5)))
    clips.append(add_text('meu3', (960, 850), (8.7)))
    clips.append(add_text('meu4', (970, 870), (11), 1))
    clips.append(add_text('meu5', (990, 890), (14.5)))
    clips.append(add_text('meu6', (900, 820), (16)))
    clips.append(add_text('meu7', (950, 880), (19)))
    
    return clips


def edit_video():
    video = mpy.VideoFileClip('template.webm')
    video = mpy.vfx.mask_color(video, color=(14, 209, 75), thr=100, s=2)
    
    background = mpy.VideoFileClip('background.mp4').without_audio()
    background = mpy.vfx.resize(background, newsize=video.size)
    
    background = background.set_duration(video.duration)

    clips = [background, video] + all_texts(False)
    
    final_clip = mpy.CompositeVideoClip(clips)
    final_clip.write_videofile('output.mp4', threads=4, fps=24,
                               codec=vcodec,
                               preset=compression,
                               ffmpeg_params=["-crf",videoquality])

    video.close()

if __name__ == '__main__':
    edit_video()